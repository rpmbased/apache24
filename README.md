# apache24

apache httpd v2.4.x RPM build for CentOS 6.x

[apache - httpd](https://github.com/apache/httpd)

- Download apache rpm files for **CentOS 6x** from here:
    - [apache24 - CentOS 6x](https://gitlab.com/rpmbased/apache24/-/jobs/artifacts/master/browse?job=build-centos-6)
